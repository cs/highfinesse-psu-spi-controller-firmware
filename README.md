# Highfinesse PSU controller firmware
Firmware files for a STM32 based board to control the Highfinesse PSUs
DAC directly via SPI.

In addition to the analog control and the USB port the digital control
option of the Highfinesse PSUs has a SPI connection to the DAC exposed on
a 8P8C (ethernet) connector. This connection can be used to send faster
and more precise updates to the power supplies.

This is the firmware for a STM32 based board which acts as a intermediate
control device for this interface. It communicates with the lab control
software through a SCPI based ethernet protocol and to the power supply
through the SPI interface. It can either set the current directly, or
replay a previously uploaded sequence on external trigger pulses. Every
trigger pulse advances the next data sample in the sequence. The timing is
fully controlled by the external triggers, the MCU only listens to them
and uploads new data in between the pulses.

## Board
The current code is designed to run on a STM32F407VG as it is used on the
CamDDS boards. We hacked a board from revision 3 where the DDS and analog
components were not fitted to house the output stage instead. Pin
definitions match those of the hacked prototype.

If this works as expected the plan is to build custom PCBs with the
required components. These also add 64MBit DRAM to store sequences of up
to 4 million points. For this the STM32F427ZG is used as it includes a
DRAM controller.

## Building
This software uses make to build the project. You can simply run `make` in
the top folder and it should build a `highfinesse-psu-spi.elf`. This can
be flashed directly on the microcontroller. For a successfull build you
will need a [GCC ARM embedded toolchain][0]. If you don't want to install
it locally you can also download the latest build from the gitlab build
artifacts instead.

## Flashing
To flash the microcontroller you will need an STLINK v2 programmer. The
board houses a standard JTAG header which should be easily connected to
the programmer.

There are multiple tools out there which can be used to transfer the data.
This project uses [st-link][1], but any other can be used if preferred. If
st-link is installed and the programmer is connected simply run `make
flash` to upload the latest code to the controller.


## TODO
- test faster SPI rates
- test sequence mode
- add & test RAM (new board)

 [0]: https://developer.arm.com/open-source/gnu-toolchain/gnu-rm
 [1]: https://github.com/stlink-org/stlink
