# remove the # and change the path to the location of your compiler
# if it complains that it can't find any program. Most likely you'll just
# have to adjust the version numbers.
#TOOLCHAIN_PATH?=/cygdrive/c/Program\ Files\ \(x86\)/GNU\ Tools\ ARM\ Embedded/4.9\ 2015q3/bin/

AR=$(TOOLCHAIN_PATH)arm-none-eabi-ar
AS=$(TOOLCHAIN_PATH)arm-none-eabi-as
CC=$(TOOLCHAIN_PATH)arm-none-eabi-gcc
GDB=$(TOOLCHAIN_PATH)arm-none-eabi-gdb
OBJCOPY=$(TOOLCHAIN_PATH)arm-none-eabi-objcopy

CFLAGS+=-std=c99
CFLAGS+=-Wall -Wextra -ggdb -g3 -O2
# processor settings for stm32f407
CFLAGS+=-mthumb -mcpu=cortex-m4 -mfloat-abi=hard -mfpu=fpv4-sp-d16

CFLAGS+=-Werror=missing-prototypes -Werror=implicit-function-declaration

# define constant for processor type
CPPFLAGS+=-DSTM32F40_41xxx
