/*
 * List of all pins used fro regular io with their name,
 * pin number and group and their configuration.
 */
DEF_GPIO(ETHERNET_RESET, A, 3, output_pullup);

DEF_GPIO(LED_FRONT, F, 7, output);
DEF_GPIO(LED_SEQUENCE, F, 8, output);

DEF_GPIO(FRONT_BUTTON, D, 7, input_pulldown);

DEF_GPIO(TRIGGER_ENABLE, B, 4, output);
DEF_GPIO(LATCH, B, 3, output);

DEF_GPIO(TRIGGER_IN, E, 2, input);

DEF_GPIO(LED_ERROR, A, 0, output);

DEF_GPIO(OUTPUT_ENABLE, B, 7, output);
