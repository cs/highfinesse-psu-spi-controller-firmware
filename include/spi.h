#ifndef _SPI_H
#define _SPI_H

#include "util.h"

#include <stm32f4xx_spi.h>

void spi_init(uint16_t prescaler);
void spi_deinit(void);
static INLINE void spi_send_byte(uint8_t data);
static INLINE void spi_send_single(uint8_t data);
static INLINE void spi_send_multiple(uint8_t* data, uint32_t length);
static INLINE void spi_wait_done(void);

/* implementation starts here */

static INLINE void
spi_send_byte(uint8_t data)
{
  // wait for TXE to be set
  while (!(SPI3->SR & SPI_SR_TXE)) {
  };

  /* fill output buffer */
  SPI3->DR = data;
}

static INLINE void
spi_send_single(uint8_t data)
{
  spi_send_byte(data);
  spi_wait_done();
}

static INLINE void
spi_send_multiple(uint8_t* data, uint32_t length)
{
  for (uint32_t i = 0; i < length; i++) {
    spi_send_byte(data[i]);
  }
  spi_wait_done();
}

static INLINE void
spi_wait_done()
{
  while (!(SPI3->SR & SPI_SR_TXE))
    ;
  while (SPI3->SR & SPI_SR_BSY)
    ;
}

#endif /* _SPI_H */
