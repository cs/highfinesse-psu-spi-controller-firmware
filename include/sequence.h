#ifndef _SEQUENCE_H
#define _SEQUENCE_H

#include <stddef.h>
#include <stdint.h>

#define SEQUENCE_BUFFER_LENGTH 16384

struct sequence_buffer_t
{
  uint32_t buffer[SEQUENCE_BUFFER_LENGTH];
  size_t position;         /// index of the entry to play next
  size_t length;           /// number of valid data entries in the buffer
  uint8_t active;          /// controls if triggers do anything
  uint8_t update_on_start; /// sent the first value immediate on activation
};

extern struct sequence_buffer_t sequence_buffer;

/** enable or disable the sequence execution
 *
 * when the sequence is enabled the MCU will start to listen for external
 * triggers. On every trigger the next value in the sequence will be sent to
 * the PSU.
 *
 * If sequence_buffer.update_on_start the buffer entry pointed to by position
 * is sent immediately (behaving as an external trigger had been sent).
 *
 * @param enable
 *   whether to start or stop the execution
 */
void sequence_enable(uint32_t enable);

/** play the next sequence value if enabled
 *
 * if the sequence_buffer.active is false nothing is done
 */
void sequence_next(void);

#endif /* _SEQUENCE_H */
