/**
 * implementation of the DAC control of the HighFinesse PSUs
 *
 * Data is sent only via SPI to the DAC as specified in the operations
 * manual. Every update is a 24-bit transfer, 16 of these bits contain
 * the current value. Current is discretised over the supported current
 * range.
 *
 * For every transfer the latch pin must be pulled high and remain high
 * until the end of the transfer. On the falling edge the new data is
 * activated by the DAC. Waiting for a reply whether it is okay to stall.
 */
#ifndef _HIGHFINESSE_H
#define _HIGHFINESSE_H

#include <stdbool.h>
#include <stdint.h>

#define DAC_BITS 18

/** initialise the control module
 *
 * Initialises the SPI module and sends the startup code to the device
 */
void highfinesse_init(void);

/**send the startup code to the supply */
void highfinesse_startup(void);

/** change the trigger mode between internal and external
 *
 *  When external triggering is enabled the latch signal is set to low
 *  (i.e. transmitting data) and not modified by the MCU anymore.
 *  External trigger pulses get inverted and pull the latch low for the
 *  duration of the pulse. The rising edge of the trigger finishes the
 *  previous communication cycle, activating the transmitted values. The
 *  falling edge starts the new cycle and the MCU immediately transferes
 *  the next data value to the power supply.
 *
 * @param enable
 *    If true external trigger mode is enabled.
 */
void highfinesse_trigger_external(bool enable);

/** Set the PSU current to the given value
 *
 * @param current
 *   Target current in the format requested by the device,
 *   inverted two's complement.
 *   The resulting current is dependent on the device limits.
 *   Use highfinesse_convert_current to generate this value
 *
 * @param trigger
 *   Boolean flag indicating if the MCU should also control the
 *   LATCH pin or if external triggering should be used. False
 *   indicates external trigger.
 */
void highfinesse_set_current(uint32_t current, bool trigger);

/** Convert current from a float value in A to the device representation
 *
 * @param current
 *   Current value given in A. Must be within the supported
 *   current range for the device, otherwise 0 current is returned
 * @return The current in the data format accepted by the device
 */
uint32_t highfinesse_convert_current(int min_current, int max_current,
                                     float current);

#endif // _HIGHFINESSE_H
