#ifndef _CONFIG_H
#define _CONFIG_H

#include "eeprom.h"
#include <stdint.h>

#define CONFIG_EEPROM eeprom_block1

struct ethernet_config
{
  uint8_t address[4];
  uint8_t submask[4];
  uint8_t gateway[4];
  uint32_t dhcp_enabled;
};

struct device_config
{
  int16_t min_current;
  int16_t max_current;
};

struct config
{
  struct ethernet_config ethernet;
  struct device_config device;
};

extern const struct config default_config;

const struct config* config_verify(void);
static INLINE const struct config* config_get(void);
void config_write(const struct config*);

static INLINE const struct config*
config_get(void)
{
  return eeprom_get(CONFIG_EEPROM, sizeof(uint32_t));
}

#endif /* _CONFIG_H */
