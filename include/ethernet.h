#include <sys/cdefs.h>
#ifndef __ETHERNET_H
#define __ETHERNET_H

#include <lwip/err.h>
#include <stddef.h>
#include <stdint.h>

void ethernet_init(void);
_Noreturn void ethernet_loop(void);

/**
 * queue data for transmision to the client. If you can guarantee that the
 * data will be valid until end of send (e.g. forever) use queue,
 * otherwise use copy_queue. */
err_t ethernet_queue(const char*, uint16_t length);
err_t ethernet_copy_queue(const char*, uint16_t length);

/** copy the next bytes directly into the specified buffer
 *
 * this function copies data directly from the ethernet receive buffer
 * to the destination without passing it somewhere else for further
 * processing. It is used to read continuous blocks of arbitrary data
 * while bypassing the scpi parser.
 *
 * This function starts to copy data at the current parser offset. When
 * reading ARB data no further modifications to the offset are necessary
 * as the parser sets it to the end of the ARB prefix by default.
 *
 * When necessary this function will wait for more packets to arrive until
 * the requested amount of data has been assembled.
 *
 * @param dest
 *   beginning of the destination memory location
 * @param len
 *   total amount of memory to copy
 *
 * @return
 * the number of bytes copied
 */
size_t ethernet_copy_data(void* dest, size_t len);

/**
 * function to set the MAC address for the device. We use the unique
 * device ID stored in the chip. The function is called from
 * lib/lwip/port/STM32F4x7/Standalone/ethernetif.c
 */
struct netif;
void set_mac_address(uint8_t*);

#endif /* __ETHERNET_H */
