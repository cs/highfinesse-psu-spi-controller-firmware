/*
 * Interrupts used and assigned priorities: Lower priorities preempt
 * interrupts with higher priority.
 *
 * +----------+----------+---------------------------------------------+
 * | Priority | IRQ      | Usage                                       |
 * +----------+----------+---------------------------------------------+
 * | 0        | EXTI1    | External trigger                            |
 * | 1        | SysTick  | 1ms system timer                            |
 * | 2        | ETH      | Ethernet packet received                    |
 * | 3        | TODO     | Ethernet periodic handling                  |
 * +----------+----------+---------------------------------------------+
 */

#ifndef _INTERRUPTS_H
#define _INTERRUPTS_H

void interrupts_init(void);

#endif /* _INTERRUPTS_H */
