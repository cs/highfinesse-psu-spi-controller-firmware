#include "config.h"
#include "ethernet.h"
#include "gpio.h"
#include "highfinesse.h"
#include "interrupts.h"
#include "timing.h"

int
main(void)
{
  /* At this stage the microcontroller clock setting is already
   * configured, this is done through SystemInit() function which is
   * called from startup file (startup_stm32f4xx.s) before to branch to
   * application main. To reconfigure the default setting of SystemInit()
   * function, refer to system_stm32f4xx.c file */

  /* initialize timers */
  sysclock_init();

  gpio_init();

  gpio_set_high(LED_ERROR);
  gpio_set_low(OUTPUT_ENABLE);

  //config_write(&default_config);

  if (gpio_get_input(FRONT_BUTTON)) {
      config_write(&default_config);
  } else {
      config_verify();
  }

  highfinesse_init();

  interrupts_init();

  ethernet_init();

  ethernet_loop();
}
