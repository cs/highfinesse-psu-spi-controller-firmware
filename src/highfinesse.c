#include "highfinesse.h"

#include "gpio.h"
#include "spi.h"

static void send_message(uint32_t, bool);

void
highfinesse_init(void)
{
  // sysclock / prescaler == spi speed
  // 7MHz should be supported according to datasheet
  // this would be 32 (5.25), use 64 for now to be on the safe side
  spi_init(SPI_BaudRatePrescaler_32);

  highfinesse_startup();
}

void
highfinesse_startup(void)
{
  gpio_set_low(LATCH);
  highfinesse_trigger_external(false);

  // initialisation sequence as specified in the data sheet
  send_message(0xBFFFFB, true);
  send_message(0xBFFFFF, true);
  send_message(0xDFFFC1, true);
  send_message(0xEFFFFF, true);
  send_message(0xDFFFCD, true);
}

void
highfinesse_trigger_external(bool enable)
{
  if (enable) {
    gpio_set_high(TRIGGER_ENABLE);
    gpio_set_high(LATCH);
  } else {
    gpio_set_low(LATCH);
    gpio_set_low(TRIGGER_ENABLE);
  }
}

void
highfinesse_set_current(uint32_t current, bool trigger)
{
  current = ~current;
  current &= (1 << DAC_BITS) - 1;
  uint32_t data = 0xE00000 | (current << (20 - DAC_BITS));

  send_message(data, trigger);
}

uint32_t
highfinesse_convert_current(int min_current, int max_current, float current)
{
  // check boundaries, we return the value for 0A in case of an invalid value
  if ((current < min_current) || (current > max_current))
    current = 0;

  float amp_per_bit =
    ((float)(max_current - min_current)) / ((1 << DAC_BITS) - 1);
  uint32_t discrete_current = (current - min_current) / amp_per_bit;
  return discrete_current;
}

/** send single message to DAC
 *
 * @param data
 *   data to send, only bits 0..23 are used, the high byte is ignored
 */
static void
send_message(uint32_t data, bool trigger)
{
  // perform byteswap and invert for correct transmission
  data = ((data >> 24) & 0xff) | ((data << 8) & 0xff0000) |
         ((data >> 8) & 0xff00) | ((data << 24) & 0xff000000);
  /* data = ~data; */

  data = data >> 8;

  if (trigger)
    gpio_set_high(LATCH);
  spi_send_multiple((uint8_t*)&data, 3);

  for (volatile int i = 0; i < 10; i++) {
    __NOP();
  }

  if (trigger)
    gpio_set_low(LATCH);
}
