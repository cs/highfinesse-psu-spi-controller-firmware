#include "sequence.h"
#include "gpio.h"
#include "highfinesse.h"

struct sequence_buffer_t sequence_buffer = {
  .buffer = {},
  .length = 0,
  .position = 0,
  .active = 0,
  .update_on_start = 0,
};

void
sequence_enable(uint32_t enable)
{
  // check if we really change the state
  if (enable == sequence_buffer.active)
    return;

  if (enable) {
	gpio_set_high(LED_FRONT);
    if (sequence_buffer.update_on_start &&
        sequence_buffer.length > sequence_buffer.position) {
      highfinesse_set_current(sequence_buffer.buffer[sequence_buffer.position],
                              true);
      sequence_buffer.position++;
    }

    highfinesse_trigger_external(true);

    sequence_buffer.active = true;

    // immediately transfer the first data entry
    sequence_next();
  } else {
	gpio_set_low(LED_FRONT);
    sequence_buffer.active = false;
    highfinesse_trigger_external(false);
  }
}

void
sequence_next(void)
{
  gpio_toggle(LED_SEQUENCE);
  if (!sequence_buffer.active)
    return;

  if (sequence_buffer.position >= sequence_buffer.length) {
    // we disable the sequence mode here as we know that we uploaded the
    // last sample point on the previous trigger pulse and the current
    // trigger pulse has activated that sample. If we would stop the
    // sequence earlier we would also disable external triggering and
    // would play the last sample to early.
    // It doesn't hurt to do this on every extra trigger
    sequence_enable(false);

    return;
  }

  highfinesse_set_current(sequence_buffer.buffer[sequence_buffer.position],
                          false);
  sequence_buffer.position++;
}
