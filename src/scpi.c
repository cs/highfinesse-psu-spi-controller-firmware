#include "scpi.h"

#include "config.h"
#include "ethernet.h"
#include "highfinesse.h"
#include "sequence.h"
#include "util.h"

#define USE_FULL_ERROR_LIST 1

#include <math.h>
#include <scpi/scpi.h>
#include <stdio.h>

#define SCPI_PATTERNS_BOTH(F)                                                  \
  F("CURrent:MINimum", current_minimum)                                        \
  F("CURrent:MAXimum", current_maximum)                                        \
  F("SEQuence[:ENable]", sequence_enable)                                      \
  F("SEQuence:POSition", sequence_position)                                    \
  F("SEQuence:UPDateonstart", sequence_updateonstart)                          \
  F("SYSTem:NETwork:ADDRess", system_network_address)                          \
  F("SYSTem:NETwork:GATEway", system_network_gateway)                          \
  F("SYSTem:NETwork:SUBmask", system_network_submask)                          \
  F("SYSTem:NETwork:DHCP", system_network_dhcp)

#define SCPI_PATTERNS_NO_QUERY(F)                                              \
  F("CURrent", current)                                                        \
  F("SEQuence:CLEAr", sequence_clear)                                          \
  F("SEQuence:DATa", sequence_data)

#define SCPI_PATTERNS_ONLY_QUERY(F)                                            \
  F("SEQuence:LENgth", sequence_length)                                        \
  F("SEQuence:LENgth:MAXimum", sequence_length_maximum)                        \
  F("SYSTem:NETwork:MAC", system_network_mac)

#define SCPI_PATTERNS(F)                                                       \
  SCPI_PATTERNS_BOTH(F##_QUERY)                                                \
  SCPI_PATTERNS_BOTH(F##_SET)                                                  \
  SCPI_PATTERNS_NO_QUERY(F##_SET)                                              \
  SCPI_PATTERNS_ONLY_QUERY(F##_QUERY)

#define SCPI_CALLBACK_LIST_SET(pattrn, clbk)                                   \
  { .pattern = pattrn, .callback = scpi_callback_##clbk },
#define SCPI_CALLBACK_LIST_QUERY(pattrn, clbk)                                 \
  { .pattern = pattrn "?", .callback = scpi_callback_##clbk##_q },

#define SCPI_CALLBACK_PROTOTYPE_SET(pattrn, clbk)                              \
  static scpi_result_t scpi_callback_##clbk(scpi_t*);
#define SCPI_CALLBACK_PROTOTYPE_QUERY(pattrn, clbk)                            \
  static scpi_result_t scpi_callback_##clbk##_q(scpi_t*);

SCPI_PATTERNS(SCPI_CALLBACK_PROTOTYPE)

static scpi_error_t scpi_error_queue_data[SCPI_ERROR_QUEUE_SIZE];

static const scpi_command_t scpi_commands[] = {
  /* IEEE Mandated Commands (SCPI std V1999.0 4.1.1) */
  { .pattern = "*CLS", .callback = SCPI_CoreCls },
  { .pattern = "*ESE", .callback = SCPI_CoreEse },
  { .pattern = "*ESE?", .callback = SCPI_CoreEseQ },
  { .pattern = "*ESR?", .callback = SCPI_CoreEsrQ },
  { .pattern = "*IDN?", .callback = SCPI_CoreIdnQ },
  { .pattern = "*OPC", .callback = SCPI_CoreOpc },
  { .pattern = "*OPC?", .callback = SCPI_CoreOpcQ },
  { .pattern = "*RST", .callback = SCPI_CoreRst },
  { .pattern = "*SRE", .callback = SCPI_CoreSre },
  { .pattern = "*SRE?", .callback = SCPI_CoreSreQ },
  { .pattern = "*STB?", .callback = SCPI_CoreStbQ },
  { .pattern = "*WAI", .callback = SCPI_CoreWai },

  /* Required SCPI commands (SCPI std V1999.0 4.2.1) */
  { .pattern = "SYSTem:ERRor[:NEXT]?", .callback = SCPI_SystemErrorNextQ },
  { .pattern = "SYSTem:ERRor:COUNt?", .callback = SCPI_SystemErrorCountQ },
  { .pattern = "SYSTem:VERSion?", .callback = SCPI_SystemVersionQ },

  SCPI_PATTERNS(SCPI_CALLBACK_LIST) SCPI_CMD_LIST_END
};

static int scpi_error(scpi_t* context, int_fast16_t err);
static size_t scpi_write(scpi_t* context, const char* data, size_t len);
static scpi_result_t scpi_reset(scpi_t*);

static scpi_bool_t scpi_param_boolean(scpi_t*, uint32_t*);
static scpi_bool_t scpi_param_current(scpi_t*, float*);
static scpi_bool_t scpi_param_ip_address(scpi_t*, uint8_t[4]);

static scpi_result_t scpi_print_current(scpi_t*, float);
static scpi_result_t scpi_print_ip_address(scpi_t*, const uint8_t[4]);
static scpi_result_t scpi_print_unit(scpi_t*, float, scpi_unit_t);

/* this struct defines the main communication functions used by the
 * library. Write is mandatory, all others are optional */
static scpi_interface_t scpi_interface = {
  .control = NULL,
  .error = scpi_error,
  .flush = NULL,
  .reset = scpi_reset,
  .write = scpi_write,
};

char device_id[25] = { 0 };

/* this struct contains all necessary information for the SCPI library */
static scpi_t scpi_context;

static int
scpi_error(scpi_t* context, int_fast16_t err)
{
  char buf[512];

  int len = snprintf(buf, sizeof(buf), "**ERROR: %d, \"%s\"", (int16_t)err,
                     SCPI_ErrorTranslate(err));
  ethernet_copy_queue(buf, len);

  return 0;
}

static size_t
scpi_write(scpi_t* context, const char* data, size_t len)
{
  (void)context;

  ethernet_copy_queue(data, len);

  return len;
}

/* perform a system reset */
static scpi_result_t
scpi_reset(scpi_t* context)
{
  highfinesse_startup();

  return SCPI_RES_OK;
}

void
scpi_init()
{
  const uint8_t* const id_unique = (const uint8_t*)0x1FFF7A10;
  for (int i = 0; i < 24; i += 2) {
    snprintf(device_id + i, 2, "%hhx", id_unique[i]);
  }

  SCPI_Init(&scpi_context, scpi_commands, &scpi_interface, scpi_units_def,
            "LOREM-IPSUM", "HighFinesse-PSU-SPI", device_id, str(GIT_REF), NULL,
            0, scpi_error_queue_data, SCPI_ERROR_QUEUE_SIZE);
}

int
scpi_process(char* data, int len)
{
  return SCPI_Parse(&scpi_context, data, len);
}

static scpi_bool_t
scpi_param_boolean(scpi_t* context, uint32_t* out)
{
  scpi_bool_t value;
  if (!SCPI_ParamBool(context, &value, true)) {
    return FALSE;
  }

  *out = !!value;

  return TRUE;
}

static scpi_bool_t
scpi_param_current(scpi_t* context, float* value)
{
  scpi_number_t number;

  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &number, TRUE)) {
    return FALSE;
  }

  if (number.special) {
    switch (number.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      case SCPI_NUM_MIN:
        *value = config_get()->device.min_current;
        return TRUE;
      case SCPI_NUM_MAX:
        *value = config_get()->device.max_current;
        return TRUE;
    }
  } else {
    if (number.unit == SCPI_UNIT_AMPER) {
      *value = number.value;
      return TRUE;
    } else {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return FALSE;
    }
  }
}

static scpi_bool_t
scpi_param_ip_address(scpi_t* context, uint8_t target[4])
{
  const char* input;
  size_t len;
  if (!SCPI_ParamCharacters(context, &input, &len, TRUE)) {
    SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
    return FALSE;
  }

  if (len > 15) {
    SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
    return FALSE;
  }

  unsigned temp[4] = { 0 };
  size_t current = 0;

  for (size_t i = 0; i < len; ++i) {
    if (input[i] == '.') {
      current += 1;
      if (current > 4) {
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      }
    } else if (input[i] < '0' || input[i] > '9') {
      SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
      return FALSE;
    } else {
      temp[current] *= 10;
      temp[current] += input[i] - '0';
      if (temp[current] > 255) {
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      }
    }
  }

  if (current != 3) {
    SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
    return FALSE;
  }

  for (size_t i = 0; i < 4; ++i) {
    target[i] = temp[i];
  }

  return TRUE;
}

static scpi_result_t
scpi_callback_current(scpi_t* context)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  float current;

  if (value.special) {
    switch (value.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return SCPI_RES_ERR;
      case SCPI_NUM_MIN:
        current = config_get()->device.min_current;
        break;
      case SCPI_NUM_MAX:
        current = config_get()->device.max_current;
        break;
    }
  } else {
    if (value.unit == SCPI_UNIT_AMPER) {
      if (value.value < config_get()->device.min_current ||
          value.value > config_get()->device.max_current) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return SCPI_RES_ERR;
      }

      current = value.value;
    } else if (value.unit == SCPI_UNIT_NONE ||
               value.unit == SCPI_UNIT_UNITLESS) {
      if (value.value < 0 || value.value >= (1 << DAC_BITS)) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return SCPI_RES_ERR;
      }

      highfinesse_set_current((uint32_t)value.value, true);
      return SCPI_RES_OK;
    } else {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return SCPI_RES_ERR;
    }
  }

  highfinesse_set_current(
    highfinesse_convert_current(config_get()->device.min_current,
                                config_get()->device.max_current, current),
    true);
  return SCPI_RES_OK;
}

static scpi_result_t
scpi_print_current(scpi_t* context, float value)
{
  return scpi_print_unit(context, value, SCPI_UNIT_AMPER);
}

static scpi_result_t
scpi_print_ip_address(scpi_t* context, const uint8_t source[4])
{
  char buf[16];

  int len = snprintf(buf, sizeof(buf), "%hhu.%hhu.%hhu.%hhu", source[0],
                     source[1], source[2], source[3]);

  if (len < 0) {
    return SCPI_RES_ERR;
  }

  SCPI_ResultCharacters(context, buf, len);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_print_unit(scpi_t* context, float value, scpi_unit_t unit)
{
  char buf[64] = { 0 };

  scpi_number_t number = {
    .special = 0,
    .value = value,
    .unit = unit,
    .base = 10,
  };

  size_t len = SCPI_NumberToStr(context, scpi_special_numbers_def, &number, buf,
                                sizeof(buf) - 1);

  SCPI_ResultCharacters(context, buf, len);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_current_maximum(scpi_t* context)
{
  float value;

  if (!scpi_param_current(context, &value)) {
    return SCPI_RES_ERR;
  }
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));
  conf.device.max_current = nearbyintf(value);

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_current_maximum_q(scpi_t* context)
{
  return scpi_print_current(context, config_get()->device.max_current);
}

static scpi_result_t
scpi_callback_current_minimum(scpi_t* context)
{
  float value;

  if (!scpi_param_current(context, &value)) {
    return SCPI_RES_ERR;
  }
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));
  conf.device.min_current = nearbyintf(value);

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_current_minimum_q(scpi_t* context)
{
  return scpi_print_current(context, config_get()->device.min_current);
}

static scpi_result_t
scpi_callback_sequence_clear(scpi_t* context)
{
  sequence_buffer.position = 0;
  sequence_buffer.length = 0;

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_data(scpi_t* context)
{
  const char* ptr;
  size_t len;
  if (!SCPI_ParamArbitraryBlock(context, &ptr, &len, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (len > SEQUENCE_BUFFER_LENGTH * sizeof(sequence_buffer.buffer[0])) {
    SCPI_ErrorPush(context, SCPI_ERROR_TOO_MUCH_DATA);
    return SCPI_RES_ERR;
  }

  len = ethernet_copy_data(sequence_buffer.buffer, len);

  sequence_buffer.position = 0;
  sequence_buffer.length = len / sizeof(sequence_buffer.buffer[0]);

  SCPI_ResultUInt32(context, len);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_enable(scpi_t* context)
{
  uint32_t value;
  if (!scpi_param_boolean(context, &value)) {
    return SCPI_RES_ERR;
  }

  sequence_enable(value);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_enable_q(scpi_t* context)
{
  SCPI_ResultBool(context, sequence_buffer.active);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_length_q(scpi_t* context)
{
  SCPI_ResultUInt32(context, sequence_buffer.length);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_length_maximum_q(scpi_t* context)
{
  SCPI_ResultUInt32(context, (uint32_t)SEQUENCE_BUFFER_LENGTH);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_position(scpi_t* context)
{
  uint32_t pos;
  if (!SCPI_ParamUInt32(context, &pos, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (pos > sequence_buffer.length) {
    SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
    return SCPI_RES_ERR;
  }

  sequence_buffer.position = pos;

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_position_q(scpi_t* context)
{
  SCPI_ResultUInt32(context, sequence_buffer.position);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_updateonstart(scpi_t* context)
{
  uint32_t value;
  if (!scpi_param_boolean(context, &value)) {
    return SCPI_RES_ERR;
  }

  sequence_buffer.update_on_start = value;

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_updateonstart_q(scpi_t* context)
{
  SCPI_ResultBool(context, sequence_buffer.update_on_start);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_address(scpi_t* context)
{
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));

  if (!scpi_param_ip_address(context, conf.ethernet.address)) {
    return SCPI_RES_ERR;
  }

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_submask(scpi_t* context)
{
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));

  if (!scpi_param_ip_address(context, conf.ethernet.submask)) {
    return SCPI_RES_ERR;
  }

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_gateway(scpi_t* context)
{
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));

  if (!scpi_param_ip_address(context, conf.ethernet.gateway)) {
    return SCPI_RES_ERR;
  }

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_dhcp(scpi_t* context)
{
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));

  if (!scpi_param_boolean(context, &conf.ethernet.dhcp_enabled)) {
    return SCPI_RES_ERR;
  }

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_address_q(scpi_t* context)
{
  return scpi_print_ip_address(context, config_get()->ethernet.address);
}

static scpi_result_t
scpi_callback_system_network_submask_q(scpi_t* context)
{
  return scpi_print_ip_address(context, config_get()->ethernet.submask);
}

static scpi_result_t
scpi_callback_system_network_gateway_q(scpi_t* context)
{
  return scpi_print_ip_address(context, config_get()->ethernet.gateway);
}

static scpi_result_t
scpi_callback_system_network_dhcp_q(scpi_t* context)
{
  SCPI_ResultBool(context, config_get()->ethernet.dhcp_enabled);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_mac_q(scpi_t* context)
{
  uint8_t mac[6];
  char buf[sizeof(mac) * 3] = { 0 };
  set_mac_address(mac);

  int len = snprintf(buf, sizeof(buf), "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", mac[0],
                     mac[1], mac[2], mac[3], mac[4], mac[5]);

  if (len < 0) {
    return SCPI_RES_ERR;
  }

  SCPI_ResultCharacters(context, buf, len);

  return SCPI_RES_OK;
}
