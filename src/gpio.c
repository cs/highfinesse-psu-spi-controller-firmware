#include "gpio.h"

#include <stm32f4xx_rcc.h>

static void gpio_init_internal(gpio_pin, uint8_t mode, uint8_t out_type,
                               uint8_t speed, uint8_t pull);
static void gpio_init_output(gpio_pin);
static void gpio_init_output_pulldown(gpio_pin);
static void gpio_init_output_pullup(gpio_pin);
static void gpio_init_input(gpio_pin);
static void gpio_init_input_pulldown(gpio_pin);
static void gpio_init_input_pullup(gpio_pin);
static void gpio_change_pin_mode(uint8_t mode, GPIO_TypeDef* GPIOx,
                                 uint16_t pinpos);
static void gpio_blink_forever(uint32_t cycles, gpio_pin);

void
gpio_init()
{
  /* enable GPIO clocks for all output modules */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);

/* initialise all gpios defined in the matching version file to their
 * default modes */
#define DEF_GPIO(name, _group, _pin, _mode) gpio_init_##_mode(name);
#include "pins.h"
#undef DEF_GPIO
}

static void
gpio_init_internal(gpio_pin pin, uint8_t mode, uint8_t out_type, uint8_t speed,
                   uint8_t pull)
{
  GPIO_InitTypeDef init = {
    .GPIO_Pin = 1 << pin.pin,
    .GPIO_Mode = mode,
    .GPIO_Speed = speed,
    .GPIO_OType = out_type,
    .GPIO_PuPd = pull,
  };
  GPIO_Init(pin.group, &init);
}

void
gpio_set_pin_mode_input(gpio_pin pin)
{
  gpio_change_pin_mode(GPIO_Mode_IN, pin.group, pin.pin);
}

void
gpio_set_pin_mode_output(gpio_pin pin)
{
  gpio_change_pin_mode(GPIO_Mode_OUT, pin.group, pin.pin);
}

void
gpio_blink_forever_slow(gpio_pin pin)
{
  gpio_blink_forever(20 * 1000 * 1000, pin);
}

void
gpio_blink_forever_fast(gpio_pin pin)
{
  gpio_blink_forever(2 * 1000 * 1000, pin);
}

static void
gpio_init_output(gpio_pin pin)
{
  gpio_init_internal(pin, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_High_Speed,
                     GPIO_PuPd_NOPULL);
}

static void
gpio_init_output_pulldown(gpio_pin pin)
{
  gpio_init_internal(pin, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_High_Speed,
                     GPIO_PuPd_DOWN);
}

static void
gpio_init_output_pullup(gpio_pin pin)
{
  gpio_init_internal(pin, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_High_Speed,
                     GPIO_PuPd_UP);
}

static void
gpio_init_input(gpio_pin pin)
{
  gpio_init_internal(pin, GPIO_Mode_IN, GPIO_OType_PP, GPIO_High_Speed,
                     GPIO_PuPd_NOPULL);
}

static void
gpio_init_input_pulldown(gpio_pin pin)
{
  gpio_init_internal(pin, GPIO_Mode_IN, GPIO_OType_PP, GPIO_High_Speed,
                     GPIO_PuPd_DOWN);
}

static void
gpio_init_input_pullup(gpio_pin pin)
{
  gpio_init_internal(pin, GPIO_Mode_IN, GPIO_OType_PP, GPIO_High_Speed,
                     GPIO_PuPd_UP);
}

static void
gpio_change_pin_mode(uint8_t mode, GPIO_TypeDef* GPIOx, uint16_t pinpos)
{
  /* remove any upper bits */
  mode &= 0x3;

  uint32_t tmp = GPIOx->MODER;
  tmp &= ~((uint32_t)(0x03 << (2 * pinpos)));
  tmp |= ((uint32_t)(mode << (2 * pinpos)));
  GPIOx->MODER = tmp;
}

static void
gpio_blink_forever(uint32_t cycles, gpio_pin pin)
{
  for (;;) {
    gpio_toggle(pin);
    for (volatile unsigned int i = 0; i < cycles; ++i) {
    }
  }
}
