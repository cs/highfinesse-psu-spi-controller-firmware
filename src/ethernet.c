#include "ethernet.h"
#include <sys/cdefs.h>

#include "config.h"
#include "gpio.h"
#include "scpi.h"
#include "timing.h"
#include "util.h"

#include <ethernetif.h>
#include <lwip/dhcp.h>
#include <lwip/stats.h>
#include <lwip/tcp.h>
#include <lwip/tcp_impl.h>
#include <lwip/udp.h>
#include <misc.h>
#include <netif/etharp.h>
#include <stm32f4x7_eth.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_syscfg.h>
#include <string.h>

//#define DP83848_PHY_ADDRESS 0x01 /* Relative to STM324xG-EVAL Board */
#define DP83848_PHY_ADDRESS 0x01 /* Test */

enum server_states
{
  ES_NONE = 0,
  ES_ACCEPTED,
  ES_CLOSING,
  ES_READING,
};

struct parser_state
{
  char buffer[128];
  char* data;
  size_t len;
};

/**
 * custom structure containing all connection details. Gets passed to all
 * callback functions as first parameter (arg). State information relevant
 * for the protocol are stored in an extra struct which we reference here.
 */
struct server_state
{
  uint8_t state;
  struct tcp_pcb* pcb;
  struct pbuf* pin;
  size_t pin_offset;
  struct parser_state parser;
};

static struct server_state es = {
  .state = ES_NONE,
  .pcb = NULL,
  .pin = NULL,
  .pin_offset = 0,
};

static struct netif gnetif;
static struct tcp_pcb* g_pcb;

void ETH_IRQHandler(void);

static void ethernet_gpio_init(void);
static void ethernet_dma_init(void);
static void ethernet_interrupt_init(void);
static void ethernet_link_callback(struct netif*);
static void lwip_init(void);
static void lwip_periodic_handle(uint32_t localtime);

static void ethernet_clear_packet(void);
static void ethernet_wait_for_packet(void);
static int ethernet_read_next_packet(void);
static void ethernet_fill_buffer(size_t);

static int server_init(void);
static err_t server_accept_callback(void* arg, struct tcp_pcb* newpcb,
                                    err_t err);
static err_t server_recv_callback(void* arg, struct tcp_pcb* pcb,
                                  struct pbuf* p, err_t err);
static void server_err_callback(void* arg, err_t err);
static err_t server_poll_callback(void* arg, struct tcp_pcb* pcb);
static err_t server_sent_callback(void* arg, struct tcp_pcb* pcb, u16_t len);
static void server_connection_close(struct tcp_pcb* pcb);

/** interrupt handler for ethernet events
 *
 * we configure it to trigger on receive events
 */
void
ETH_IRQHandler()
{
  if (ETH_GetDMAITStatus(ETH_DMA_IT_R)) {
    /* we have received a package interrupt, check that we really have a frame
     */
    if (ETH_CheckFrameReceived()) {
      ethernetif_input(&gnetif);
    }

    ETH_DMAClearITPendingBit(ETH_DMA_IT_R);
  }
}

void
ethernet_init()
{
  es.parser.data = es.parser.buffer;
  es.parser.len = 0;

  /* reset pin is active low */
  gpio_set_low(ETHERNET_RESET);
  for (volatile int i = 0; i < 1000; ++i) {
  }
  gpio_set_high(ETHERNET_RESET);

  ethernet_gpio_init();

  ethernet_dma_init();

  lwip_init();

  server_init();

  scpi_init();

  /* enable interrupts last to make sure we can handle them properly */
  ethernet_interrupt_init();
}

static void
ethernet_gpio_init()
{
  /* Enable GPIOs clocks */
//  RCC_AHB1PeriphClockCmd(
//    RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC, ENABLE);

  /* Enable SYSCFG clock */
//  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

  /* MII/RMII Media interface selection --------------------------------------*/
  SYSCFG_ETH_MediaInterfaceConfig(SYSCFG_ETH_MediaInterface_RMII);

  /* Configure PA1, PA2 and PA7 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_7;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_ETH);

  /* Configure PB11, PB12 and PB13 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource12, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_ETH);

  /* Configure PC1, PC4 and PC5 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_4 | GPIO_Pin_5;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource1, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource4, GPIO_AF_ETH);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource5, GPIO_AF_ETH);
}

err_t
ethernet_queue(const char* data, uint16_t length)
{
  if (es.pcb == NULL || es.state == ES_CLOSING) {
    return 0;
  }

  if (length == 0) {
    length = strlen(data);
  }

  return tcp_write(es.pcb, data, length, 0);
}

err_t
ethernet_copy_queue(const char* data, uint16_t length)
{
  if (es.pcb == NULL || es.state == ES_CLOSING) {
    return 0;
  }

  if (length == 0) {
    length = strlen(data);
  }

  return tcp_write(es.pcb, data, length, TCP_WRITE_FLAG_COPY);
}

size_t
ethernet_copy_data(void* dest, size_t len)
{
  size_t i = 0;
  while (i < len) {
    size_t copy_len = min(es.pin->len - es.pin_offset, len - i);
    memcpy(dest + i, es.pin->payload + es.pin_offset, copy_len);
    i += copy_len;

    /* keep track of how much data we've read of the packet in case there
     * are commands behind the arb data */
    es.pin_offset += copy_len;
    es.parser.data += copy_len;
    es.parser.len -= copy_len;

    if (i < len) {
      if (ethernet_read_next_packet() < 0) {
        return i;
      }
      es.parser.data = es.pin->payload + es.pin_offset;
      es.parser.len = es.pin->len - es.pin_offset;
    }
  }

  return i;
}

/** find the end of the next command
 *
 * @param data beginning of the search
 * @param len max search len
 * @return positive distance from data or 0 if nothing was found. Negative
 * values indicate missing bytes to form a complete command
 */
static int
find_command_end(char* data, size_t len)
{
  for (char* cur = data; cur + 1 < data + len; cur++) {
    if (*cur == ';' && *(cur + 1) == ':') {
      return cur - data + 1;
    } else if (*cur == '\r' && *(cur + 1) == '\n') {
      return cur - data + 2;
    } else if (*cur == '#') {
      // ARB data, also include the range specifier
      uint8_t len_bytes = *(cur + 1) - '0';
      if (len_bytes == 0 || len_bytes > 9)
        return 0;
      int missing_bytes = (int)len - (cur - data) - (2 + len_bytes);
      if (missing_bytes < 0)
        return missing_bytes;
      return cur - data + 2 + len_bytes;
    }
  }
  return 0;
}

static void
process_buffer()
{
  struct parser_state* state = &es.parser;

  for (;;) {
    int cmd_len = 0;
    if (state->len > 0) {
      cmd_len = find_command_end(state->data, state->len);
    }

    if (cmd_len == 0) {
      ethernet_fill_buffer(0);
    } else if (cmd_len < 0) {
      ethernet_fill_buffer(-cmd_len);
    } else {
      // update positions before processing as it might exchange the
      // buffers when reading arb data
      char* current = state->data;
      if (state->data != state->buffer) {
        es.pin_offset += cmd_len;
      }
      state->data += cmd_len;
      state->len -= cmd_len;

      // process command
      scpi_process(current, cmd_len);
    }
  }
}

/**
 * search for command end
 * if found -> return length
 *
 * not found
 * - at end
 *  -> grab new package, buf=eth
 *  -> return self (tail-recursion)
 *
 * leftover in package
 * - buf=buffer
 * - append leftover to buffer
 * loop:
 *   - grab new package,
 *  )
 *
 *
 */

/** fetch new data from ethernet to get a new command to parse
 *
 * when this function is called the currently defined buffer does not hold
 * enough data for a complete command. Once it is finished it should hold
 * more data, hopefully containing a full command.
 *
 * @param len number of bytes to read. If it is 0 the function will search
 * for the next command end and read that many bytes
 */
static void
ethernet_fill_buffer(size_t len)
{
  struct parser_state* state = &es.parser;
  size_t current_len = state->len;

  if (current_len <= 0) {
    /* previous package perfectly done, continue with the next data from
     * ethernet (could be in the same packet */

    if (state->data != state->buffer) {
      /* done with the current packet */
      ethernet_clear_packet();
    }

    ethernet_wait_for_packet();
    state->data = es.pin->payload + es.pin_offset;
    state->len = es.pin->len;

    return;
  }
  if (state->data == state->buffer) {
    /* we were processing from the buffer, but should be finished now as
     * we only fill it with exactly one command. Switch back to ethernet
     * now */
    ethernet_wait_for_packet();
    state->data = es.pin->payload + es.pin_offset;
    state->len = es.pin->len;

    return;
  }

  /* currently processing data from an ethernet buffer, data wraps
   * around a packet boundary. Copy remaining data to the temporary
   * buffer and fill it with more data from the network */

  if (current_len > sizeof(state->buffer)) {
    // remaining data does not fit in our temp package. Return an error
    // and drop the current packet
    ethernet_queue("Temp buffer too small", 0);
    ethernet_clear_packet();
    goto error;
  }

  /* copy leftover data to the internal buffer */
  state->data = state->buffer;
  memcpy(state->data, es.pin->payload + es.pin_offset,
         es.pin->len - es.pin_offset);
  state->len = current_len;

  int missing = len;
  while (1) {
    if (ethernet_read_next_packet() != 0) {
      /* connection closed */
      goto error;
    }

    size_t copy_len;

    if (missing > 0) {
      /* we want a fixed amount */
      copy_len = min(missing, (int)(es.pin->len - es.pin_offset));
    } else {
      /* search for command end, copy until then */
      int new_end = find_command_end(es.pin->payload + es.pin_offset,
                                     es.pin->len - es.pin_offset);
      if (new_end <= 0) {
        copy_len = es.pin->len - es.pin_offset;
        if (new_end < 0) {
          missing = copy_len - new_end;
        }
      } else {
        copy_len = new_end;
      }
    }

    /* packet does not have an end, append to buffer (if it fits), fetch more
     */
    if (sizeof(state->buffer) - state->len < copy_len) {
      /* buffer too small */
      ethernet_queue("Temp buffer too small", 0);
      goto error;
    }
    memcpy(state->data + state->len, es.pin->payload + es.pin_offset, copy_len);
    es.pin_offset += copy_len;
    state->len += copy_len;
    missing -= copy_len;

    if (missing <= 0)
      return;
  }

error:
  state->data = state->buffer;
  state->len = 0;
}

_Noreturn void
ethernet_loop()
{
  for (;;) {
	gpio_set_low(LED_ERROR);
    ethernet_wait_for_packet();
    process_buffer();
    tcp_output(es.pcb);
    gpio_set_high(LED_ERROR);
  }
}

static void
ethernet_dma_init()
{
  /* Enable ETHERNET clock  */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_ETH_MAC | RCC_AHB1Periph_ETH_MAC_Tx |
                           RCC_AHB1Periph_ETH_MAC_Rx,
                         ENABLE);

  /* Reset ETHERNET on AHB Bus */
  ETH_DeInit();

  /* Software reset */
  ETH_SoftwareReset();

  /* Wait for software reset */
  while (ETH_GetSoftwareResetStatus() == SET)
    ;

  ETH_InitTypeDef ETH_InitStructure;
  /* ETHERNET Configuration
   * --------------------------------------------------*/
  /* Call ETH_StructInit if you don't like to configure all ETH_InitStructure
   * parameter */
  ETH_StructInit(&ETH_InitStructure);

  /* Fill ETH_InitStructure parametrs */
  /*------------------------   MAC   -----------------------------------*/
  ETH_InitStructure.ETH_AutoNegotiation = ETH_AutoNegotiation_Enable;
  ETH_InitStructure.ETH_Speed = ETH_Speed_100M;
  ETH_InitStructure.ETH_Mode = ETH_Mode_FullDuplex;

  ETH_InitStructure.ETH_LoopbackMode = ETH_LoopbackMode_Disable;
  ETH_InitStructure.ETH_RetryTransmission = ETH_RetryTransmission_Disable;
  ETH_InitStructure.ETH_AutomaticPadCRCStrip = ETH_AutomaticPadCRCStrip_Disable;
  ETH_InitStructure.ETH_ReceiveAll = ETH_ReceiveAll_Disable;
  ETH_InitStructure.ETH_BroadcastFramesReception =
    ETH_BroadcastFramesReception_Enable;
  ETH_InitStructure.ETH_PromiscuousMode = ETH_PromiscuousMode_Disable;
  ETH_InitStructure.ETH_MulticastFramesFilter =
    ETH_MulticastFramesFilter_Perfect;
  ETH_InitStructure.ETH_UnicastFramesFilter = ETH_UnicastFramesFilter_Perfect;
  ETH_InitStructure.ETH_ChecksumOffload = ETH_ChecksumOffload_Enable;

  /*------------------------   DMA   -----------------------------------*/

  /* When we use the Checksum offload feature, we need to enable the Store and
  Forward mode:
  the store and forward guarantee that a whole frame is stored in the FIFO, so
  the MAC can insert/verify the checksum,
  if the checksum is OK the DMA can handle the frame otherwise the frame is
  dropped */
  ETH_InitStructure.ETH_DropTCPIPChecksumErrorFrame =
    ETH_DropTCPIPChecksumErrorFrame_Enable;
  ETH_InitStructure.ETH_ReceiveStoreForward = ETH_ReceiveStoreForward_Enable;
  ETH_InitStructure.ETH_TransmitStoreForward = ETH_TransmitStoreForward_Enable;

  ETH_InitStructure.ETH_ForwardErrorFrames = ETH_ForwardErrorFrames_Disable;
  ETH_InitStructure.ETH_ForwardUndersizedGoodFrames =
    ETH_ForwardUndersizedGoodFrames_Disable;
  ETH_InitStructure.ETH_SecondFrameOperate = ETH_SecondFrameOperate_Enable;
  ETH_InitStructure.ETH_AddressAlignedBeats = ETH_AddressAlignedBeats_Enable;
  ETH_InitStructure.ETH_FixedBurst = ETH_FixedBurst_Enable;
  ETH_InitStructure.ETH_RxDMABurstLength = ETH_RxDMABurstLength_32Beat;
  ETH_InitStructure.ETH_TxDMABurstLength = ETH_TxDMABurstLength_32Beat;
  ETH_InitStructure.ETH_DMAArbitration = ETH_DMAArbitration_RoundRobin_RxTx_2_1;

  /* Configure Ethernet */
  ETH_Init(&ETH_InitStructure, DP83848_PHY_ADDRESS);
}

static void
ethernet_interrupt_init(void)
{
  NVIC_InitTypeDef NVIC_InitStructure = { .NVIC_IRQChannel = ETH_IRQn,
                                          .NVIC_IRQChannelPreemptionPriority =
                                            2,
                                          .NVIC_IRQChannelSubPriority = 0,
                                          .NVIC_IRQChannelCmd = ENABLE };

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
  NVIC_Init(&NVIC_InitStructure);

  /* enable the receive interrupt */
  ETH_DMAITConfig(ETH_DMA_IT_R, ENABLE);
}

static void
ethernet_link_callback(struct netif* netif)
{
  if (netif_is_link_up(netif)) {
    /* Restart the autonegotiation */
    /* Reset Timeout counter */
    uint32_t timeout = 0;

    /* Enable Auto-Negotiation */
    ETH_WritePHYRegister(DP83848_PHY_ADDRESS, PHY_BCR, PHY_AutoNegotiation);

    /* Wait until the auto-negotiation will be completed */
    do {
      timeout++;
    } while (!(ETH_ReadPHYRegister(DP83848_PHY_ADDRESS, PHY_BSR) &
               PHY_AutoNego_Complete) &&
             (timeout < (uint32_t)PHY_READ_TO));

    /* Reset Timeout counter */
    timeout = 0;

    /* Read the result of the auto-negotiation */
    uint32_t RegValue = ETH_ReadPHYRegister(DP83848_PHY_ADDRESS, PHY_SR);

    uint32_t ETH_Mode;
    /* Configure the MAC with the Duplex Mode fixed by the auto-negotiation
     * process */
    if ((RegValue & PHY_DUPLEX_STATUS) != (uint32_t)RESET) {
      /* Set Ethernet duplex mode to Full-duplex following the
       * auto-negotiation */
      ETH_Mode = ETH_Mode_FullDuplex;
    } else {
      /* Set Ethernet duplex mode to Half-duplex following the
       * auto-negotiation */
      ETH_Mode = ETH_Mode_HalfDuplex;
    }

    uint32_t ETH_Speed;
    /* Configure the MAC with the speed fixed by the auto-negotiation process
     */
    if (RegValue & PHY_SPEED_STATUS) {
      /* Set Ethernet speed to 10M following the auto-negotiation */
      ETH_Speed = ETH_Speed_10M;
    } else {
      /* Set Ethernet speed to 100M following the auto-negotiation */
      ETH_Speed = ETH_Speed_100M;
    }

    /*------------------------ ETHERNET MACCR Re-Configuration
     * --------------------*/
    /* Get the ETHERNET MACCR value */
    uint32_t tmpreg = ETH->MACCR;

    /* Set the FES bit according to ETH_Speed value */
    /* Set the DM bit according to ETH_Mode value */
    tmpreg |= (ETH_Speed | ETH_Mode);

    /* Write to ETHERNET MACCR */
    ETH->MACCR = (uint32_t)tmpreg;

    _eth_delay_(ETH_REG_WRITE_DELAY);
    tmpreg = ETH->MACCR;
    ETH->MACCR = tmpreg;

    /* Restart MAC interface */
    ETH_Start();

    struct ip_addr ipaddr;
    struct ip_addr netmask;
    struct ip_addr gw;
    const struct ethernet_config* eth_conf = &(config_get()->ethernet);

    if (!eth_conf->dhcp_enabled) {
      IP4_ADDR(&ipaddr, eth_conf->address[0], eth_conf->address[1],
               eth_conf->address[2], eth_conf->address[3]);
      IP4_ADDR(&netmask, eth_conf->submask[0], eth_conf->submask[1],
               eth_conf->submask[2], eth_conf->submask[3]);
      IP4_ADDR(&gw, eth_conf->gateway[0], eth_conf->gateway[1],
               eth_conf->gateway[2], eth_conf->gateway[3]);

    } else {
      IP4_ADDR(&ipaddr, 0, 0, 0, 0);
      IP4_ADDR(&netmask, 0, 0, 0, 0);
      IP4_ADDR(&gw, 0, 0, 0, 0);
    }
    netif_set_addr(&gnetif, &ipaddr, &netmask, &gw);

    /* When the netif is fully configured this function must be called.*/
    netif_set_up(&gnetif);

    if (eth_conf->dhcp_enabled) {
      dhcp_start(&gnetif);
    }
  } else {
    ETH_Stop();

    /*  When the netif link is down this function must be called.*/
    netif_set_down(&gnetif);
  }
}

static void
lwip_init()
{
  struct ip_addr ipaddr;
  struct ip_addr netmask;
  struct ip_addr gw;

  /* Initializes the dynamic memory heap defined by MEM_SIZE.*/
  mem_init();

  /* Initializes the memory pools defined by MEMP_NUM_x.*/
  memp_init();

  /* Initializes the pbuf memory pool defined by PBUF_POOL_SIZE */
  pbuf_init();

  /* Initializes the ARP table and queue. */
  etharp_init();

  ip_init();
  tcp_init();
  udp_init();

  /* - netif_add(struct netif *netif, struct ip_addr *ipaddr,
  struct ip_addr *netmask, struct ip_addr *gw,
  void *state, err_t (* init)(struct netif *netif),
  err_t (* input)(struct pbuf *p, struct netif *netif))

  Adds your network interface to the netif_list. Allocate a struct
  netif and pass a pointer to this structure as the first argument.
  Give pointers to cleared ip_addr structures when using DHCP,
  or fill them with sane numbers otherwise. The state pointer may be NULL.

  The init function pointer must point to a initialization function for
  your ethernet netif interface. The following code illustrates it's use.*/
  netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init,
            &ethernet_input);

  /*  Registers the default network interface.*/
  netif_set_default(&gnetif);

  if (ETH_ReadPHYRegister(DP83848_PHY_ADDRESS, PHY_SR) & 1) {
    /* Set Ethernet link flag */
    gnetif.flags |= NETIF_FLAG_LINK_UP;

    /* When the netif is fully configured this function must be called.*/
    netif_set_up(&gnetif);
  } else {
    /*  When the netif link is down this function must be called.*/
    netif_set_down(&gnetif);
  }

  /* Set the link callback function, this function is called on change of link
   * status*/
  netif_set_link_callback(&gnetif, ethernet_link_callback);

  /* call it manually to perform initial configuration */
  ethernet_link_callback(&gnetif);
}

/**
 * Call at lwIP timer functions in the correct intervals. We use LocalTime
 * as reference which increases every millisecond
 */
static void
lwip_periodic_handle(uint32_t localtime)
{
  static uint32_t tcp_fast_timer = 0;
  static uint32_t tcp_slow_timer = 0;
  static uint32_t arp_timer = 0;
  static uint32_t link_timer = 0;
  static uint32_t dhcp_fast_timer = 0;
  static uint32_t dhcp_slow_timer = 0;
  static int link_status = 0;

  if (localtime - link_timer >= 1000) {
    link_timer = localtime;

    /* the normal solution to detect link changes is via an extra line in
     * the MII interface. Our system however uses RMII, which doesn't have
     * that line. To replace that we periodicaly poll the link status bit
     * in the registers of the PHYTER */
    int new_status = ETH_ReadPHYRegister(DP83848_PHY_ADDRESS, PHY_SR) & 1;
    if (link_status != new_status) {
      link_status = new_status;
      if (link_status == 1) {
        netif_set_link_up(&gnetif);
      } else {
        netif_set_link_down(&gnetif);
      }
    }
  }

  if (localtime - tcp_fast_timer >= TCP_FAST_INTERVAL) {
    tcp_fast_timer = localtime;
    tcp_fasttmr();
  }

  if (localtime - tcp_slow_timer >= TCP_SLOW_INTERVAL) {
    tcp_slow_timer = localtime;
    tcp_slowtmr();
  }

  if (localtime - arp_timer >= ARP_TMR_INTERVAL) {
    arp_timer = localtime;
    etharp_tmr();
  }

  if (localtime - dhcp_slow_timer >= DHCP_COARSE_TIMER_MSECS) {
    dhcp_slow_timer = localtime;
    dhcp_coarse_tmr();
  }

  if (localtime - dhcp_fast_timer >= DHCP_FINE_TIMER_MSECS) {
    dhcp_fast_timer = localtime;
    dhcp_fine_tmr();
  }
}

/**
 * This function starts the server and causes the program to listen on
 * port 5024 waiting for incoming connections.
 * From 33500B: 5024 SCPI telnet, 5025 SCPI socket
 */
static int
server_init()
{
  g_pcb = tcp_new();

  if (g_pcb == NULL) {
    gpio_blink_forever_fast(LED_SEQUENCE);
    return 1;
  }

  err_t err = tcp_bind(g_pcb, IP_ADDR_ANY, 5024);

  if (err != ERR_OK) {
    memp_free(MEMP_TCP_PCB, g_pcb);
    gpio_blink_forever_slow(LED_SEQUENCE);
    return err;
  }

  g_pcb = tcp_listen(g_pcb);

  tcp_accept(g_pcb, server_accept_callback);

  return 0;
}

/**
 * This function is called when a client opens a new connction to our
 * server.
 *
 * @param  arg: not used
 * @param  newpcb: the pcb of the new connection
 * @param  err: not used
 * @retval err_t: error code
 */
static err_t
server_accept_callback(void* arg, struct tcp_pcb* newpcb, err_t err)
{
  LWIP_UNUSED_ARG(arg);
  LWIP_UNUSED_ARG(err);

  // close any old connections before accepting the next
  if (es.state != ES_NONE) {
    tcp_close(es.pcb);
  }

  es.state = ES_ACCEPTED;
  es.pcb = newpcb;
  es.pin = NULL;
  es.pin_offset = 0;

  tcp_arg(newpcb, &es);

  tcp_recv(newpcb, server_recv_callback);

  tcp_sent(newpcb, server_sent_callback);

  tcp_err(newpcb, server_err_callback);

  tcp_poll(newpcb, server_poll_callback, 1);

  tcp_accepted(newpcb);

  return ERR_OK;
}

/**
 * This function implements the tcp_recv LwIP callback. It get's called
 * every time a packet is received.
 *
 * @param arg: pointer on the self defined struct server_state
 * @param pcb: pointer on the tcp_pcb connection
 * @param pbuf: pointer on the received pbuf
 * @param err: error information regarding the received pbuf
 * @retval err_t: error code
 */
static err_t
server_recv_callback(void* arg, struct tcp_pcb* pcb, struct pbuf* p, err_t err)
{
  /* if we receive an empty tcp frame from the client we close the
   * connection */
  if (p == NULL) {
    es.state = ES_CLOSING;

    server_connection_close(pcb);

    return ERR_OK;
  }

  /* a non empty frame was received, but for some reason err != ERR_OK */
  if (err != ERR_OK) {
    /* free received pbuf */
    pbuf_free(p);
    return err;
  }

  /* data from client */
  if (es.pin != NULL) {
    /* chain original and new data */
    pbuf_chain(es.pin, p);
  } else {
    es.pin = p;
    es.pin_offset = 0;
  }

  return ERR_OK;
}

/**
 * This function implements the tcp_err callback. It get's called when a
 * fatal tcp_connection error occurs.
 */
static void
server_err_callback(void* arg, err_t err)
{
  LWIP_UNUSED_ARG(err);

  if (arg != NULL) {
    mem_free(arg);
  }
}

/**
 * This function implements the tcp_poll LwIP callback. This function gets
 * called in regular intervalls by the library
 *
 * @param arg: pointer on the self defined struct server_state
 * @param pcb: pointer on the tcp_pcb connection
 * @retval err_t: error code
 */
static err_t
server_poll_callback(void* arg, struct tcp_pcb* pcb)
{
  if (es.state == ES_CLOSING) {
    server_connection_close(pcb);
  }
  return ERR_OK;
}

/**
 * This function implements the tcp_sent LwIP callback. It is called every
 * time an ACK is received from the remote host for sent data.
 *
 * @param arg: pointer on the self defined struct server_state
 * @param pcb: pointer on the tcp_pcb connection
 * @param len: reported lentgh of received data
 * @retval err_t: error code
 */
static err_t
server_sent_callback(void* arg, struct tcp_pcb* pcb, u16_t len)
{
  LWIP_UNUSED_ARG(len);

  if (es.state == ES_CLOSING) {
    server_connection_close(pcb);
  }

  return ERR_OK;
}

static void
server_connection_close(struct tcp_pcb* pcb)
{
  if (pcb == NULL) {
    return;
  }

  /* remove all callbacks */
  tcp_arg(pcb, NULL);
  tcp_sent(pcb, NULL);
  tcp_recv(pcb, NULL);
  tcp_err(pcb, NULL);
  tcp_poll(pcb, NULL, 0);

  /* close connection */
  tcp_close(pcb);

  es.pcb = NULL;
  es.state = ES_NONE;
}

static void
ethernet_clear_packet()
{
  /* free the previous packet */
  struct pbuf* ptr = es.pin;

  if (ptr == NULL)
    return;

  /* apparently we have worked our way through the data, signal this to
   * the TCP stack */
  tcp_recved(es.pcb, ptr->len);

  es.pin = es.pin->next;
  es.pin_offset = 0;

  if (es.pin != NULL) {
    pbuf_ref(es.pin);
  }

  pbuf_free(ptr);
}

/** make sure we have an ethernet package to process
 *
 * The new package will be in es.pin. This function is safe to call even
 * if we already have a package. If it returns it is guaranteed that
 * es.pin != NULL;
 */
static void
ethernet_wait_for_packet()
{
  do {
    if (ETH_CheckFrameReceived()) {
      /* Read a received packet from the Ethernet buffers and send it to the
       * lwIP for handling */
      ethernetif_input(&gnetif);
    }

    lwip_periodic_handle(LocalTime);
  } while (es.pin == NULL);
}

/** mark the current packet as processed and fetch a new packet
 *
 * @returns 0 if a package has been loaded, -1 if the connection was
 * aborted
 */
static int
ethernet_read_next_packet()
{
  ethernet_clear_packet();

  /* force transmission of the ACK, otherwise the remote might not send
   * the next data package */
  es.pcb->flags |= TF_ACK_NOW;
  tcp_output(es.pcb);

  es.state = ES_READING;

  do {
    if (ETH_CheckFrameReceived()) {
      /* Read a received packet from the Ethernet buffers and send it to the
       * lwIP for handling */
      ethernetif_input(&gnetif);
    }

    lwip_periodic_handle(LocalTime);

    /* if the remote hast closed the connection we can't read any more
     * data from this connection */
    if (es.state != ES_READING && es.pin == NULL) {
      return -1;
    }
  } while (es.pin == NULL);

  return 0;
}

void
set_mac_address(uint8_t* mac)
{
  /* magic address at which the unique device ID can be found */
  const uint8_t* const id_unique = (const uint8_t*)0x1FFF7A10;

  /* give the devices a common prefix */
  mac[0] = 0x42;
  mac[1] = 0x42;

  // combine the bits of the unique_id to generate a random mac address
  for (int i = 0; i < 4; ++i) {
    mac[i + 2] = *(id_unique + i) ^ *(id_unique + i + 4) ^ *(id_unique + i + 8);
  }

  /* specify as localy administrated address */
  mac[0] |= 0x2;
  /* specifiy as unicast address */
  mac[0] &= ~0x1;
}
