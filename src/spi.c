#include "spi.h"

#include <stm32f4xx_gpio.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_spi.h>

/**
 */

/**
 * this functions sets up SPI3 for communication with the HighFinesse
 * PSU. As it only supports writing data we disable receive.
 */
void
spi_init(uint16_t prescaler)
{
  SPI_InitTypeDef spi_init;

  SPI_StructInit(&spi_init);

  /* enable SPI clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI3, ENABLE);

  /* init pins */
  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.GPIO_Speed = GPIO_High_Speed;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_12;
  GPIO_Init(GPIOC, &GPIO_InitStructure);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_SPI3);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_SPI3);

  /* set options */
  spi_init.SPI_DataSize = SPI_DataSize_8b;
  spi_init.SPI_BaudRatePrescaler = prescaler;
  spi_init.SPI_Direction = SPI_Direction_1Line_Tx;
  spi_init.SPI_FirstBit = SPI_FirstBit_MSB;
  spi_init.SPI_Mode = SPI_Mode_Master;
  spi_init.SPI_NSS = SPI_NSS_Soft;
  /* SPI mode */
  spi_init.SPI_CPOL = SPI_CPOL_High;
  spi_init.SPI_CPHA = SPI_CPHA_2Edge;

  /* disable first */
  SPI_Cmd(SPI3, DISABLE);

  SPI_Init(SPI3, &spi_init);

  /* enable SPI */
  SPI_Cmd(SPI3, ENABLE);
}

void
spi_deinit()
{
  spi_wait_done();

  SPI_DeInit(SPI3);
}
